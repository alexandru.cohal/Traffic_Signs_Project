# Computer Vision Project
***
### Detection and Recognition of Traffic Signs
***
Term project within the *TDT4265 - Computer Vision* course from *Norwegian University of Science and Technology (NTNU)*, developed in the *Summer Semester 2016-2017*  
  
Group 34: Alexandru Cohal, Yu He

Detect and Recognize traffic signs - 'Traditional' Computer Vision and Deep Learning methods

Based on German Traffic Sign Recognition and Detection Benchmarks: http://benchmark.ini.rub.de/
